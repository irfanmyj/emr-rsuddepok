<?php get_template('inc/navbar-page.html'); ?>
	<!-- accordion -->
	<div class="collapse-page segments-page">
		<div class="container">
			<div class="wrap-title">
				<h5><?php echo strtoupper($title);?></h5>
			</div>
			<div class="content no-mb" style="padding: 5px;">
				<div class="form-group">
					<!-- <input type="text" id="myInput" onkeyup="search()" placeholder="" class="form-control"> -->
				</div>
			</div>

			<div class="wrap-content">
				<div id="accordionPage1" class="accordion b-shadow">
					<?php
					$no = 0; 
					foreach ($judul as $k => $v) { 
					$no++;
					?>
						<div class="card">
							<div id="header<?php echo $no;?>" class="card-header" data-toggle="collapse" data-target="#collapse<?php echo $no;?>">
								<h6> <?php echo $no .'. '. $v;?></h6>
							</div>
							<div id="collapse<?php echo $no;?>" class="collapse" aria-labelledby="header<?php echo $no;?>" data-parent="#accordionPage1">
								<div class="card-body">
									<span><?php echo $isi[$k]; ?></span>
								</div>
							</div>
						</div>
					<?php } ?>
					
				</div>

				<div id="showtext" style="display: none;">
					
				</div>
			</div>
		</div>
	</div>
	<!-- end accordion -->
	<?php get_template('inc/footer.html'); ?>
	<script type="text/javascript">
		/*function search() {
		  // Declare variables 
		  var input, filter, table, tr, td, i, txtValue;
		  input = document.getElementById("myInput");
		  filter = input.value.toUpperCase();
		  table = document.getElementById("accordionPage1");
		  tr = table.getElementsByTagName("div");
		  // Loop through all table rows, and hide those who don't match the search query
		  for (i = 0; i < tr.length; i++) {
		    td = tr[i].getElementsByTagName("div")[0];
			    if (td) {
			      txtValue = td.textContent || td.innerText;
			      if (txtValue.toUpperCase().indexOf(filter) > -1) {
			      	document.getElementById('showtext').innerHTML = td.innerText;
			        document.getElementById('showtext').style.display = "block";
			      } else {
			        tr[i].style.display = "none";
			      }
			    } 
		  }
		}*/
	</script>
	<?php get_template('inc/endhtml.html'); ?>