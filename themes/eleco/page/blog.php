	<?php get_template('inc/navbar-page.html'); global $Cf;?>
	<!-- slide -->
	<div class="slide slide-blog-list">
		<div class="slide-show owl-carousel owl-theme">
			<?php foreach($rr as $k => $v) { ?>
			<div class="slide-content">
				<div class="mask"></div>
				<img src="<?php echo $Cf->_file_akses.'/'.$v->gambar; ?>" alt="">
				<div class="intro-caption">
					<h6>Artikel Terpopuler</h6>
					<a href="<?php echo base_url('artikel/detail/'.$v->id_artikel.'/'.str_replace(' ', '', $v->judul)); ?>"><h5><?php echo $v->judul; ?></h5></a>
					<p class="date"><i class="fa fa-clock-o"></i><?php echo date('d M Y', strtotime($v->masuk_tgl)); ?></p>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
	<!-- end slide -->

	<!-- latest news -->
	<div class="latest-news segments">
		<div class="container">
			<?php foreach($r as $k => $v) { ?>
				<div class="row">
					<div class="col s5">
						<div class="content-image">
							<img src="<?php echo $Cf->_file_akses.'/'.$v->gambar; ?>" alt="<?php echo $v->judul; ?>">
						</div>
					</div>
					<div class="col s7">
						<div class="content-text">
							<span>Artikel - by RSUD KOTA DEPOK</span>
							<a href="<?php echo base_url('artikel/detail/'.$v->id_artikel.'/'.str_replace(' ', '', $v->judul)); ?>"><h5><?php echo $v->judul; ?></h5></a>
							<p class="date"><i class="fa fa-clock-o"></i><?php echo date('d M Y', strtotime($v->masuk_tgl)); ?></p>
						</div>
					</div>
				</div>
			<?php } ?>
			<nav aria-label="Page navigation example">
				<?php echo $this->pagination->create_links();?>
				<!-- <ul class="pagination">
					<li class="page-item disabled"><a class="page-link" href="">1</a></li>
					<li class="page-item"><a class="page-link" href="">2</a></li>
					<li class="page-item"><a class="page-link" href="">3</a></li>
					<li class="page-item"><a class="page-link" href="">4</a></li>
				</ul> -->
			</nav>
		</div>
	</div>
	<!-- end latest news -->
	<?php get_template('inc/footer.html'); ?>
	<?php get_template('inc/endhtml.html'); ?>