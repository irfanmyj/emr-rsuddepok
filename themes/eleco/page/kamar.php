<?php get_template('inc/navbar-page.html'); ?>
<div class="table-page segments-page">
        <div class="container">
            <div class="wrap-title">
                <h5><?php echo strtoupper($title); ?> | <?php echo strtoupper(tanggal_indo(date('Y-m-d H:i:s')));?></h5>
            </div>
            <div class="wrap-content b-shadow">
                <table class="table table-striped" >
                    <thead>
                        <tr>
                            <th>Kelas Kamar</th>
                            <th style="text-align:center;">Jumlah Bed Tersedia</th>
                            <th style="text-align:center;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php  
                    foreach ($r as $k => $v) {
                        echo '<tr style="cursor:pointer" data-kd="'.$v->nama.'" data-kd_bangsal="'.$v->kd_bangsal.'">';
                            echo '<td width="720" class="kamar_detail">';
                                echo $kamar[$v->nama];
                            echo '</td>'; 
                            echo '<td class="kamar_detail" align="center"><span class="badge badge-primary">'.$v->total.'</span></td>'; 
                            echo '<td width="200" class="kamar_detail"><button class="btn btn-primary">Lihat Detail</button></td>';
                           echo '</tr>'; 
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="content" style="display: none;" id="kamarshow">
            <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">KETERSEDIAAN TEMPAT TIDUR RSUD KOTA DEPOK</h5>
                            <button class="close" data-dismiss="modal" aria-label="close">
                                <span aria-hidden="true"><i class="fa fa-close"></i></span>
                            </button>
                        </div>
                        <div class="modal-body" id="contentkamar"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<?php get_template('inc/footer.html'); ?>
<script type="text/javascript">
   $(document).ready(function(){
    var link = '<?php echo base_url('Get_ajax');?>';

    $('.kamar_detail').click(function(){
        var tr = $(this).parent();
        $.ajax({
            type    : 'post',
            url     : link+'/getDetailKamar',
            data    : 'kd='+tr.data('kd')+'&kd_bangsal='+tr.data('kd_bangsal'),
            success : function(res){
                $('#kamarshow').css('display','block');
                $('#exampleModal2').modal();
                $('#contentkamar').html(res);
            }
        });
    });
   }); 
</script>
<?php get_template('inc/endhtml.html'); ?>