<?php get_template('inc/navbar-page.html'); ?>
<!-- tabs -->
	<div class="tabs-page segments-page">
		<div class="container">
			<div class="tabs b-shadow">
				<div class="nav nav-tabs justify-content-center" id="nav-tab" role="tablist">
					<a href="#nav-tabs1" class="nav-item nav-link active" id="nav-tabs1-tab" data-toggle="tab" role="tab" aria-controls="nav-tabs1" aria-selected="true">Cari Lokasi Faskes</a>
					<a href="#nav-tabs2" class="nav-item nav-link" id="nav-tabs2-tab" data-toggle="tab" role="tab" aria-controls="nav-tabs2" aria-selected="false">Lihat Informasi Pelayanan</a>
					<a href="#nav-tabs3" class="nav-item nav-link" id="nav-tabs3-tab" data-toggle="tab" role="tab" aria-controls="nav-tabs3" aria-selected="false">
						Cek Status Kepesertaan
					</a>
				</div>
			</div>
			<div class="tab-content b-shadow" id="nav-tabContent">
				<div class="tab-pane fade show active" id="nav-tabs1" role="tabpanel" aria-labelledby="nav-tabs1-tab">
					<div class="form-element segments-page">
						<div class="container">
							<div class="content no-mb" id="spn1" style="display: none;">
								<div class="form-group">
									<button class="btn btn-primary" type="button" disabled>
									  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
									  Silahkan tunggu, data sedang di proses...
									</button>
								</div>
							</div>
							<div class="content no-mb">
								<div class="form-group">
									<h5>Tulis Nama Daerah Faskes Yang Ingin Dicari</h5>
									<input type="text" class="form-control" name="nm_daerah" id="nm_daerah">
								</div>
							</div>
							<div class="content no-mb">
								<h5>Selecct</h5>
								<select class="custom-select" name="nm_faskes" id="nm_faskes">
									<option selected>Pilih Faskes</option>
									<option value="1">Faskes 1 (Puskesmas)</option>
									<option value="2">Faskes 2 (RS)</option>
								</select>
							</div>
							<div class="content no-mb">
								<button type="submit" class="btn btn-primary" id="proses"><i class="fa fa-gear"></i> Proses</button>
							</div>
						</div>

						<div class="container" id="showFaskes"></div>
					</div>
				</div>
				<div class="tab-pane fade show" id="nav-tabs2" role="tabpanel" aria-labelledby="nav-tabs2-tab">
					<div class="form-element segments-page">
						<div class="container">
							<div class="content no-mb" id="spn2" style="display: none;">
								<div class="form-group">
									<button class="btn btn-primary" type="button" disabled>
									  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
									  Silahkan tunggu, data sedang di proses...
									</button>
								</div>
							</div>
							<div class="content no-mb">
									<div class="form-group">
										<h5>Masukan No Kartu BPJS</h5>
										<input type="text" class="form-control" name="no_kartu" id="no_kartu">
									</div>
								</div>

								<div class="content no-mb">
									<div class="form-group">
										<h5>Tanggal Awal</h5>
										<input type="date" class="form-control" name="tgl_awal" id="tgl_awal">
									</div>
								</div>

								<div class="content no-mb">
									<div class="form-group">
										<h5>Tanggal Akhir</h5>
										<input type="date" class="form-control" name="tgl_akhir" id="tgl_akhir">
									</div>
								</div>

								<div class="content no-mb">
									<button type="submit" class="btn btn-primary" id="prosesPelayanan"><i class="fa fa-gear"></i> Proses</button>
								</div>
						</div>

						<div class="container" id="showPelayanan"></div>
					</div>
				</div>
				
				<div class="tab-pane fade show" id="nav-tabs3" role="tabpanel" aria-labelledby="nav-tabs3-tab">
					<div class="form-element segments-page">
						<div class="container">
							<div class="content no-mb" id="spn3" style="display: none;">
								<div class="form-group">
									<button class="btn btn-primary" type="button" disabled>
									  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
									  Silahkan tunggu, data sedang di proses...
									</button>
								</div>
							</div>
								<div class="content no-mb">
									<div class="form-group">
										<h5>Masukan No Induk KTP</h5>
										<input type="text" class="form-control" name="no_nik" id="no_nik">
									</div>
								</div>

								<div class="content no-mb">
									<div class="form-group">
										<h5>Tanggal Pelayanan</h5>
										<input type="date" class="form-control" name="tgl_pelayanan" id="tgl_pelayanan">
									</div>
								</div>

								<div class="content no-mb">
									<button type="submit" class="btn btn-primary" id="prosesKepesertaan"><i class="fa fa-gear"></i> Proses</button>
								</div>
						</div>

						<div class="container" id="showKepesertaan"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end tabs -->
<?php get_template('inc/footer.html'); ?>
<script type="text/javascript">
	$(document).ready(function(){
		var link = '<?php echo base_url('Get_ajax');?>';
		$('#proses').click(function(){
			$('#spn1').css('display','block');
			var nm_daerah = $('#nm_daerah').val();
			var nm_faskes = $('#nm_faskes').val();
			if(nm_daerah != '' && nm_faskes!='')
			{
				$.ajax({
					type	: 'post',
					url 	: link+'/getFakses',
					data 	: 'nm_daerah='+nm_daerah+'&nm_faskes='+nm_faskes,
					success	: function(res){
						$('#spn1').css('display','none');
						$('#showFaskes').show(1000);
						$('#showFaskes').html(res);
					}
				});
			}
			else
			{
				$('#spn1').css('display','none');
				alert('Mohon maaf form yang anda kirim kosong.');
			}
		});

		$('#prosesPelayanan').click(function(){
			$('#spn2').css('display','block');
			var no_kartu = $('#no_kartu').val();
			var tgl_awal = $('#tgl_awal').val();
			var tgl_akhir = $('#tgl_akhir').val();
			if(no_kartu != '' && tgl_awal!='' && tgl_akhir!='')
			{
				$.ajax({
					type	: 'post',
					url 	: link+'/getPelayanan',
					data 	: 'no_kartu='+no_kartu+'&tgl_awal='+tgl_awal+'&tgl_akhir='+tgl_akhir,
					success	: function(res){
						$('#spn2').css('display','none');
						$('#showPelayanan').show(1000);
						$('#showPelayanan').html(res);
					}
				});
			}
			else
			{
				$('#spn2').css('display','none');
				alert('Mohon maaf form yang anda kirim kosong.');
			}
		});

		$('#prosesKepesertaan').click(function(){
			$('#spn3').css('display','block');
			var no_nik = $('#no_nik').val();
			var tgl_pelayanan = $('#tgl_pelayanan').val();
			if(no_nik != '' && tgl_pelayanan!='')
			{
				$.ajax({
					type	: 'post',
					url 	: link+'/getKepesertaan',
					data 	: 'no_nik='+no_nik+'&tgl_pelayanan='+tgl_pelayanan,
					success	: function(res){
						$('#spn3').css('display','none');
						$('#showKepesertaan').show(1000);
						$('#showKepesertaan').html(res);
					}
				});
			}
			else
			{
				$('#spn3').css('display','none');
				alert('Mohon maaf form yang anda kirim kosong.');
			}
		});

	});
</script>
<?php get_template('inc/endhtml.html'); ?>