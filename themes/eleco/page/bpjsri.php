<?php get_template('inc/navbar-page.html'); ?>
<!-- Booking Umum -->
<div class="form-element segments-page">
	<div class="container">

		<div class="content no-mb">
			<div class="form-group">
				<h5>Tanggal Periksa</h5>
				<input type="date" class="form-control" value="" name="tgl_registrasi" id="tgl_registrasi">
			</div>
		</div>

		<div class="content no-mb" id="rujukan" style="display: none;">
			<div class="form-group">
				<h5>Nomor Sep</h5>
				<input type="text" class="form-control" value="" name="no_sep" id="no_sep">
			</div>
			<!-- <div class="form-group">
				<h5>Nomor Rujukan/Nomor Kartu</h5>
				<input type="text" class="form-control" value="" name="no_rujukan" id="no_rujukan">
			</div> -->
			<!-- <div class="form-group">
				<h5>Pilih Kategori Nomor Yang di isi</h5>
				<select name="kategori" class="form-control">
					<option>-------</option>
					<option value="nokartu"> No Kartu</option>
					<option value="norujukan"> No Rujukan</option>
				</select>
			</div> -->
		</div>

		<div class="content no-mb" id="no_skdp1" style="display: none;">
			<div class="form-group">
				<h5>Nomor Surat SKDP</h5>
				<input type="text" class="form-control" value="" name="no_skdp" id="no_skdp" placeholder="Jika tidak ada no SKDP, silahkan kosongkan.">
			</div>
		</div>

		<div class="content no-mb" id="spiner" style="display: none;">
			<div class="form-group">
				<button class="btn btn-primary" type="button" disabled>
				  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				  Silahkan tunggu, data sedang di proses...
				</button>
			</div>
		</div>

		<div class="content no-mb">
			<button type="submit" class="btn btn-primary" id="tombol1"><i class="fa fa-gear"></i> Proses</button>
		</div>
	</div>
</div>

<div class="content" style="display: none;" id="modalUmum">
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">Informasi Gagal</h5>
	                <button class="close" data-dismiss="modal" aria-label="close">
	                    <span aria-hidden="true"><i class="fa fa-close"></i></span>
	                </button>
	            </div>
	            <div class="modal-body" id="msg"></div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<!-- end Booking Umum -->
<?php get_template('inc/footer.html'); ?>
<script type="text/javascript">
	$(document).ready(function(){
		var link = '<?php echo base_url('Get_ajax');?>';
		$('#tgl_registrasi').change(function(){
			var tgl_registrasi = $(this).val();
			if(tgl_registrasi)
			{
				$.ajax({
					type : 'post',
					url : link+'/umum_step1',
					data : 'tgl_registrasi='+tgl_registrasi,
					success : function(res)
					{
						var JsDt = JSON.parse(res);
						if(JsDt.msg == '')
						{
							$('#rujukan').css('display','block');	
							$('#no_skdp1').css('display','block');	
						}
						else
						{
							$('#rujukan').css('display','none');	
							$('#no_skdp1').css('display','none');	
							$('#modalUmum').css('display','block');
							$('#msg').html(JsDt.msg);
							$('#exampleModal2').modal();
						}
					}
				});
			}
		});

		$('#tombol1').click(function(){
			$('#spiner').css('display','block');
			var tgl_registrasi = $('#tgl_registrasi').val();
			var no_sep = $('#no_sep').val();
			/*var no_rujukan = $('#no_rujukan').val();*/
			var no_skdp = $('#no_skdp').val();
			//var kategori = $('[name="kategori"]').val();
			//data : 'no_sep='+no_sep+'&no_rujukan='+no_rujukan+'&tgl_registrasi='+tgl_registrasi+'&no_skdp='+no_skdp+'&kategori='+kategori,
			$.ajax({
				type : 'post',
				url : link+'/getNoSep',
				data : 'no_sep='+no_sep+'&tgl_registrasi='+tgl_registrasi+'&no_skdp='+no_skdp,
				success : function(res)
				{
					var JsDt = JSON.parse(res);
					if(JsDt.msg == '')
					{

						window.location = '<?php echo base_url('bpjs/bpjsri2');?>';
					}
					else
					{
						$('#spiner').css('display','none');
						$('#modalUmum').css('display','block');
						$('#msg').html(JsDt.msg);
						$('#exampleModal2').modal();
					}	
				}
			});
		});
	});
</script>
<?php get_template('inc/endhtml.html'); ?>