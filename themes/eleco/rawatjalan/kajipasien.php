<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/ui-lightness/jquery-ui.css" type="text/css" rel="stylesheet" />
<style type="text/css">
.nav-tabs .nav-link:not(.active) {
    border-color: transparent !important;
}
.input-sm {
	height: 30px;
}

a.active.nav-link {
	color: #ffffff;
	background-color: #28a745 !important;
}

/*.tabbable .nav-tabs {
   overflow-x: auto;
   overflow-y:hidden;
   flex-wrap: nowrap;
}*/

</style>
<?php $this->site->get_template('inc/navbar-page.html'); ?>
<!-- tabs -->
	<div class="open-hours segments-page">
		<div class="container">
			<div class="row" style="padding: 5px;">
				<div class="col-8 mb-2">
					<a href="<?php echo base_url($this->uri->segment(1).'/index');?>" class="btn btn-primary col-12 btn-sm">Kembali</a>
				</div>
				<div class="col-2 mb-2">
					<button type="button" class="btn btn-success col-12 btn-sm">
					  <span class="badge" style="font-size: 16px;" id="watch"></span>
					</button>
				</div>
				<div class="col-2 mb-2">
					<button type="button" class="btn btn-primary col-12 btn-sm" id="update_status_pasien" data-no_rawat="<?php echo $noRawat; ?>">Update Status Periksa</button>
				</div>
				<div class="col-12">
					<div class="row">
						<div class="col-8">
							<?php $this->site->get_template('rawatjalan/datapasien');  ?>
						</div>

						<?php 
						$this->site->get_template('rawatjalan/list_pasien');    
						?>
						
						<div class="container h-100 py-2">
						    <ul class="nav nav-tabs border-0" id="myTab1" role="tablist">
						        <li class="nav-item">
						            <a class="nav-link active border border-success border-bottom-0" id="riwayat_pasien-tab" data-toggle="tab" href="#riwayat_pasien" role="tab" aria-controls="riwayat_pasien" aria-selected="true">Riwayat Pasien</a>
						        </li>
						    </ul>

						    <div class="tab-content h-75 bg-white">
						        <div class="tab-pane h-100 p-3 active border border-primary" id="riwayat_pasien" role="tabpanel" aria-labelledby="riwayat_pasien-tab">
						        	<?php 
						        	$this->site->get_template('rawatjalan/form_tanggal_kunjungan'); 
						        	$this->site->get_template('rawatjalan/riwayat_pasien'); 
						        	?>
						        </div>
						    </div>
						</div>

						<div class="container h-100 py-2">
						    <!-- <nav class="tabbable"> -->
						    <ul class="nav nav-tabs border-0" id="myTab" role="tablist">
						    	<li class="nav-item">
						            <a class="nav-link active border border-success border-bottom-0" id="perawat_tabs-tab" data-toggle="tab" href="#perawat_tabs" role="tab" aria-controls="perawat_tabs" aria-selected="false">Pemeriksaan Perawat</a>
						        </li>
						        <li class="nav-item">
						            <a class="nav-link border border-success border-bottom-0" id="soap-tab" data-toggle="tab" href="#soap" role="tab" aria-controls="soap" aria-selected="true">Formulir Soap</a>
						        </li>
						        <li class="nav-item">
						            <a class="nav-link border border-success border-bottom-0" id="diagnosa-tab" data-toggle="tab" href="#diagnosa" role="tab" aria-controls="diagnosa" aria-selected="false">Formulir Diagnosa</a>
						        </li>
						        <li class="nav-item">
						            <a class="nav-link border border-success border-bottom-0" id="resepobat-tab" data-toggle="tab" href="#resepobat" role="tab" aria-controls="resepobat" aria-selected="false">Resep Obat Pasien</a>
						        </li>
						        <li class="nav-item">
						            <a class="nav-link border border-success border-bottom-0" id="laboratorium-tab" data-toggle="tab" href="#laboratorium" role="tab" aria-controls="laboratorium" aria-selected="false">Formulir Permintaan Lab</a>
						        </li>
						        <li class="nav-item">
						            <a class="nav-link border border-success border-bottom-0" id="radiologi_tabs-tab" data-toggle="tab" href="#radiologi_tabs" role="tab" aria-controls="radiologi_tabs" aria-selected="false">Formulir Permintaan Rad</a>
						        </li>
						        <li class="nav-item">
						            <a class="nav-link border border-success border-bottom-0" id="tindakan_tabs-tab" data-toggle="tab" href="#tindakan_tabs" role="tab" aria-controls="tindakan_tabs" aria-selected="false">Formulir Tindakan</a>
						        </li>
						    </ul>
						    <!-- </nav> -->
						    <div class="tab-content h-75 bg-white">
						    	<div class="tab-pane active h-100 p-3 border border-primary" id="perawat_tabs" role="tabpanel" aria-labelledby="perawat_tabs-tab">
						        	<?php $this->site->get_template('rawatjalan/datapemeriksaan');?>
						        </div>
						        <div class="tab-pane h-100 p-3 border border-primary" id="soap" role="tabpanel" aria-labelledby="soap-tab">
						        	<div class="row">
						        		<div class="col-12">
											<div class="form-group mt-2">
												<label>Pilih Tanggal Kunjungan Pasien</label>
											<?php
										    echo $this->instrument->htmlSelectFromArray($driwayats, 'name="no_rawat_soap" id="no_rawat_soap" style="width:100%;" class="form-control search_soap"', true);
										    ?>
											</div>
										</div>
										<div class="col-12 tgl_kunjungan"></div>
										<div class="col-6 border bg-light pl-2" id="history_soap_last"></div>
						        		<div class="col-6">
						        			<?php $this->site->get_template('rawatjalan/soap');?>
						        		</div>
						        	</div>
						        </div>
						        <div class="tab-pane h-100 p-3 border border-primary" id="diagnosa" role="tabpanel" aria-labelledby="diagnosa-tab">
						        	<div class="row">
							        	<div class="col-12">
											<div class="form-group mt-2">
												<label>Pilih Tanggal Kunjungan Pasien</label>
											<?php
										    echo $this->instrument->htmlSelectFromArray($driwayats, 'name="no_rawat_diagnosa" id="no_rawat_diagnosa" style="width:100%;" class="form-control search_diagnosa"', true);
										    ?>
											</div>
										</div>
										<div class="col-12 tgl_kunjungan"></div>
										<div class="col-12">
											<div class="showdiagnosa m-2">
											<?php  
											if($diagnosa){ 
												echo $diagnosa;
											} 
											?>
											</div>
										</div>
										<div class="col-6 " id="history_diagnosa_last">
						        			
						        		</div>
						        		<div class="col-6">
						        			<?php $this->site->get_template('rawatjalan/diagnosa');?>
						        		</div>
					        		</div>
						        </div>
						        <div class="tab-pane h-100 p-3 border border-primary" id="resepobat" role="tabpanel" aria-labelledby="resepobat-tab">
						        	<div class="row">
							        	<div class="col-12">
											<div class="form-group mt-2">
												<label>Pilih Tanggal Kunjungan Pasien</label>
											<?php
										    echo $this->instrument->htmlSelectFromArray($driwayats, 'name="no_rawat_obat" id="no_rawat_obat" style="width:100%;" class="form-control search_obat"', true);
										    ?>
											</div>
										</div>
										<div class="col-12 tgl_kunjungan"></div>
										<div class="col-12">
											<div class="showresepobat">
											<?php if($resepobat) { 
											echo $resepobat;
											} 
											?>
											</div>
										</div>
										<div class="col-12 " id="history_obat_last">
						        			
						        		</div>
						        		<div class="col-12">
						        			<?php $this->site->get_template('rawatjalan/obat');?>
						        		</div>
					        		</div>
						        </div>
						        <div class="tab-pane h-100 p-3 border border-primary" id="laboratorium" role="tabpanel" aria-labelledby="laboratorium-tab">
						        	<div class="row">
							        	<div class="col-12">
											<div class="form-group mt-2">
												<label>Pilih Tanggal Kunjungan Pasien</label>
											<?php
										    echo $this->instrument->htmlSelectFromArray($driwayats, 'name="no_rawat_diagnosa" id="no_rawat_laboratorium" style="width:100%;" class="form-control search_laboratorium"', true);
										    ?>
											</div>
										</div>
										<div class="col-12 tgl_kunjungan"></div>
										<div class="col-12">
											<div class="showlabs">
											<?php 
											if($reslabs)
											{ 
												echo $reslabs;
											} 
											?>
											</div>
										</div>
										<div class="col-6 " id="history_laboratorium_last">
						        			
						        		</div>
						        		<div class="col-6">
						        			<?php $this->site->get_template('rawatjalan/laboratorium');?>
						        		</div>
					        		</div>
						        </div>
						        <div class="tab-pane h-100 p-3 border border-primary" id="radiologi_tabs" role="tabpanel" aria-labelledby="radiologi_tabs-tab">
						        	<div class="row">
							        	<div class="col-12">
											<div class="form-group mt-2">
												<label>Pilih Tanggal Kunjungan Pasien</label>
											<?php
										    echo $this->instrument->htmlSelectFromArray($driwayats, 'name="no_rawat_radiologi" id="no_rawat_radiologi" style="width:100%;" class="form-control search_radiologi"', true);
										    ?>
											</div>
										</div>
										<div class="col-12 tgl_kunjungan"></div>
										<div class="col-12">
											<div class="showrads">
											<?php 
											if($resrads) { 
												echo $resrads;
											} 
											?>
											</div>
										</div>
										<div class="col-6 " id="history_radiologi_last">
						        			
						        		</div>
						        		<div class="col-6">
						        			<?php $this->site->get_template('rawatjalan/radiologi');?>
						        		</div>
					        		</div>
						        </div>
						        <div class="tab-pane h-100 p-3 border border-primary" id="tindakan_tabs" role="tabpanel" aria-labelledby="tindakan_tabs-tab">
						        	<div class="row">
							        	<div class="col-12">
											<div class="form-group mt-2">
												<label>Pilih Tanggal Kunjungan Pasien</label>
											<?php
										    echo $this->instrument->htmlSelectFromArray($driwayats, 'name="no_rawat_tindakan" id="no_rawat_tindakan" style="width:100%;" class="form-control search_radiologi"', true);
										    ?>
											</div>
										</div>
										<div class="col-12 tgl_kunjungan"></div>
										<div class="col-12">
											<div class="showtindakan">
											<?php 
											if($tindakan) { 
												echo $tindakan;
											} 
											?>
											</div>
										</div>
										<div class="col-6 " id="history_tindakan_last">
						        			
						        		</div>
						        		<div class="col-6">
						        			<?php $this->site->get_template('rawatjalan/tindakan');?>
						        		</div>
					        		</div>
						        </div>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end tabs -->
	<div class="content" style="display: none;" id="modalUmum">
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">Informasi Pesan</h5>
	                <button class="close" data-dismiss="modal" aria-label="close">
	                    <span aria-hidden="true"><i class="fa fa-close"></i></span>
	                </button>
	            </div>
	            <div class="modal-body">
	            	<p id="msg"></p>		
	            </div>
	            <div class="modal-footer">
	                <button type="button" id="tutups" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<?php $this->site->get_template('inc/footer.html'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js" type="text/javascript"></script>
<?php 
$this->site->get_template('rawatjalan/js'); 
$this->site->get_template('inc/endhtml.html'); 
?>