<?php $this->site->get_template('inc/navbar-page.html'); ?>
<div class="table-page segments-page">
		<div class="container">
			<div class="wrap-title">
				<h5><?php echo strtoupper($titled); ?></h5>
			</div>

			<div class="content">
				<div class="row">
					<div class="col-12">
						<table class="table table-striped table-responsive display" style="width:100%" id="example">
							<thead>
								<tr>
									<th>Nomor Antrian</th>
									<th>Waktu Datang</th>
									<th>No Rm</th>
									<th>Nama Pasien</th>
									<th>Tanggal Lahir</th>
									<th>Poliklinik</th>
									<th>Dokter</th>
									<th>Cara Bayar</th>
									<th>Tanggal Periksa</th>
									<th>Status</th>
									
								</tr>
							</thead>
							<tbody>
								<?php 
								foreach ($row as $k => $v) { 
								?>
								<tr style="cursor:pointer;" <?php //echo $this->instrument->showValRec($v); ?>>
									<td class="tr_mod"><?php echo $v->no_reg; ?></td>
									<td class="tr_mod"><?php echo $v->waktu_kunjungan; ?></td>
									<td class="tr_mod"><?php echo $v->no_rkm_medis; ?></td>
									<td class="tr_mod"><?php echo $v->nm_pasien; ?></td>
									<td class="tr_mod"><?php echo $v->tgl_lahir; ?></td>
									<td class="tr_mod"><?php echo $v->nm_poli; ?></td>
									<td class="tr_mod"><?php echo $this->session->userdata('nm_dokter'); ?></td>
									<td class="tr_mod"><?php echo $v->png_jawab; ?></td>
									<td class="tr_mod"><?php echo $v->tanggal_periksa; ?></td>
									<td class="tr_mod"><?php echo $v->status; ?></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<div class="col-12"><hr></div>
					<div class="col-12">
						<div class="content total_daftar_bulan" id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
					</div>

					<div class="col-12"><hr></div>

					<div class="col-12">
						<div class="content total_daftar_bulan" id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
					</div>
				</div>

				
			</div>
		</div>
</div>
<div class="content" style="display: none;" id="modalUmum">
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">Batal Booking</h5>
	                <button class="close" data-dismiss="modal" aria-label="close">
	                    <span aria-hidden="true"><i class="fa fa-close"></i></span>
	                </button>
	            </div>
	            <div class="modal-body">
	            	<p id="msg"></p>		
	            </div>
	            <div class="modal-footer">
	                <button type="button" id="tutups" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
	                <a href="<?php echo base_url('home/view');?>" type="button" id="sukses" class="btn btn-secondary" style="display: none;">Kembali</a>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<?php $this->site->get_template('inc/footer.html'); ?>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();	
});
</script>
<?php $this->site->get_template('inc/endhtml.html'); ?>