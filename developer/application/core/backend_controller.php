<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class backend_controller extends MY_Controller{	
	
	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Analisispengguna_model'); // load model class Get_model
		$this->load->library(array('Instrument','Site','form_validation','session','mybreadcrumb')); // load library
		$this->load->helper(array('menu_helper')); // load library

		// get status profiling in database
		/*$join = array(
			'bt_conmeta' => array(
				'metode' => 'INNER',
				'relasi' => 'bt_conmeta.group_id=bt_options.options_id'
			)
		);

		$field = '
			bt_options.status,
			bt_conmeta.attribute
		';

		$where  = array(
			'bt_options.options_id' => 1
		);

		$getProfiling = $this->Options_model->getJoin(NULL,$join,$field,$where)->result();
		$this->instrument->show_profiling($getProfiling[0]->status,$getProfiling[0]->attribute);*/
		
		//Config template
		$this->site->themes			= 'eleco';
		$this->site->themes_error	= 'errors';
		
		$this->site->template 		= '';
		$this->site->template_error	= 'html';

		// Breadcrumb
		$this->mybreadcrumb->add('Home', base_url('home/index'));
		$this->mybreadcrumb->add(ucwords($this->uri->segment(1)).' - '.ucwords($this->uri->segment(2)), base_url($this->uri->segment(1).'/'.$this->uri->segment(2)));
	}

}