<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemeriksaanlab_model extends MY_Model{

	protected $_table_name = 'periksa_lab';
	protected $_primary_key = 'kd_jenis_prw';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}

	private $fieldLabs = '
		periksa_lab.kd_jenis_prw,
		periksa_lab.no_rawat,
		periksa_lab.tgl_periksa,
		jns_perawatan_lab.kd_jenis_prw,
		jns_perawatan_lab.nm_perawatan';

	private $tbjoinLabs = array(
		'jns_perawatan_lab' => array(
			'metode' => 'inner',
			'relasi' => 'jns_perawatan_lab.kd_jenis_prw=periksa_lab.kd_jenis_prw'
		)
	);

	public function getPemeriksaanLab($where)
	{
		return $this->getJoin('',$this->tbjoinLabs,$this->fieldLabs,$where)->result();
	}

}