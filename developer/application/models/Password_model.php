<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Password_model extends MY_Model{

	protected $_table_name = 'user';
	protected $_primary_key = 'id_user';
	protected $_order_by = 'id_user';
	protected $_order_by_type = 'ASC';

	public $rules = array(
		'password' => array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required'
		)
	);

	public function __construct(){
		parent::__construct();
	}

}