<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemeriksaanralan_model extends MY_Model{

	protected $_table_name = 'pemeriksaan_ralan';
	protected $_primary_key = 'no_rawat';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}
}