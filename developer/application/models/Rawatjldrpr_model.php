<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rawatjldrpr_model extends MY_Model{

	protected $_table_name = 'rawat_jl_drpr';
	protected $_primary_key = 'no_rawat';
	protected $_order_by = '';
	protected $_order_by_type = '';
	protected $_database = '';

	public function __construct(){
		parent::__construct();
	}
	
	private $field = '
    	rawat_jl_drpr.kd_dokter,
    	rawat_jl_drpr.nip,
		rawat_jl_drpr.no_rawat,
		rawat_jl_drpr.kd_jenis_prw,
		rawat_jl_drpr.material,
		rawat_jl_drpr.bhp,
		rawat_jl_drpr.kso,
		rawat_jl_drpr.menejemen,
		rawat_jl_drpr.tarif_tindakanpr,
		rawat_jl_drpr.tarif_tindakandr,
		rawat_jl_drpr.biaya_rawat,
		jns_perawatan.nm_perawatan,
		kategori_perawatan.nm_kategori
	';

	private $tbjoin = array(
		'jns_perawatan' => array(
			'metode' => 'inner',
			'relasi' => 'jns_perawatan.kd_jenis_prw=rawat_jl_drpr.kd_jenis_prw'
		),
		'kategori_perawatan' => array(
			'metode' => 'inner',
			'relasi' => 'kategori_perawatan.kd_kategori=jns_perawatan.kd_kategori'
		)
	);

	public function getPermintaanTindakan($where)
	{
	   return $this->getJoin('',$this->tbjoin,$this->field,$where)->result();
	}

}