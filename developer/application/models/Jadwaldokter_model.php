<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwaldokter_model extends MY_Model{

	protected $_table_name = 'jadwal';
	protected $_primary_key = 'kd_dokter';
	protected $_order_by = '';
	protected $_order_by_type = '';
	public function __construct(){
		parent::__construct();
	}
}