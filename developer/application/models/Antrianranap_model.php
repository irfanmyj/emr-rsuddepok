<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antrianranap_model extends MY_Model{

	protected $_table_name = 'kamar_inap';
	protected $_primary_key = 'no_rawat';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}

	private $tbjoin = array(
		'reg_periksa' 	 => array(
			'metode' => 'INNER',
			'relasi' => 'reg_periksa.no_rawat=kamar_inap.no_rawat'
		),
		'pasien' 	 => array(
			'metode' => 'INNER',
			'relasi' => 'pasien.no_rkm_medis=reg_periksa.no_rkm_medis'
		),
		'kamar' => array( // nama tabel
			'metode' => 'INNER', // metode join
			'relasi' => 'kamar.kd_kamar=kamar_inap.kd_kamar' // filed relasi tabel antar tabel
		),
		'bangsal' => array( // nama tabel
			'metode' => 'INNER', // metode join
			'relasi' => 'bangsal.kd_bangsal=kamar.kd_bangsal' // filed relasi tabel antar tabel
		),
		'penjab' => array(
			'metode' => 'INNER',
			'relasi' => 'penjab.kd_pj=reg_periksa.kd_pj'
		),
		'dpjp_ranap' => array(
			'metode' => 'INNER',
			'relasi' => 'dpjp_ranap.no_rawat=reg_periksa.no_rawat'
		)
	);

	private $field = '
		reg_periksa.no_reg,
		reg_periksa.no_rawat,
		reg_periksa.no_rkm_medis,
		reg_periksa.tgl_registrasi,
		reg_periksa.jam_reg,
		reg_periksa.umurdaftar,
		reg_periksa.stts,
		bangsal.nm_bangsal,
		penjab.png_jawab,
		pasien.nm_pasien,
		pasien.tgl_lahir,
		pasien.jk
	';

	public function getPaseinRanap($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','reg_periksa.no_reg ASC',$limit,$offset)->result();
	}

}