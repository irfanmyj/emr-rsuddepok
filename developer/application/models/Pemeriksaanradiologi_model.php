<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemeriksaanradiologi_model extends MY_Model{

	protected $_table_name = 'periksa_radiologi';
	protected $_primary_key = 'kd_jenis_prw';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public function __construct(){
		parent::__construct();
	}

	private $fieldRad = '
    	periksa_radiologi.no_rawat,
		periksa_radiologi.kd_jenis_prw,
		periksa_radiologi.tgl_periksa,
		periksa_radiologi.jam,
		jns_perawatan_radiologi.kd_jenis_prw,
		jns_perawatan_radiologi.nm_perawatan
	';

	private $tbjoinRad = array(
		'jns_perawatan_radiologi' => array(
			'metode' => 'inner',
			'relasi' => 'jns_perawatan_radiologi.kd_jenis_prw=periksa_radiologi.kd_jenis_prw'
		)
	);

	public function getPemeriksaanRad($where)
	{
	   return $this->getJoin('',$this->tbjoinRad,$this->fieldRad,$where)->result();
	}

}