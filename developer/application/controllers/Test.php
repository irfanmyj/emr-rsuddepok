<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(['encryption','email']);
	}

	private $configEmail	= array(
		'protocol' => 'smtp',
		'smtp_host' => 'ssl://smtp.googlemail.com',
		'smtp_port' => 465,
		'smtp_timeout' => '7',
		'smtp_user' => 'email@gmail.com',
		'smtp_pass' => 'password',
		'charset' => 'utf-8',
		'newline' => "\r\n",
		'mailtype' => 'html'
	);
	
	public function lupapassword()
	{
		try {
				$link = base_url('auth/validasi/code_token');

			    $this->email->initialize($this->configEmail);


			    $this->email->from('email@gmail.com', 'Rsud Depok Informasi');
			    $this->email->to($datas['email']); 

			    // Lampiran email, isi dengan url/path file
		        $this->email->attach('namafile.jpg');

			    // Subject email
		        $this->email->subject('ALAMAT LINK RESET PASSWORD APLIKASI DOKTER RSUD DEPOK');
		        // Isi email
		        $this->email->message('Ini adalah email informasi reset password yang dikirm aplikasi dokter rsud depok.<br><br> Untuk memulai membuat password baru silahkan klik link ini : <strong><a href='.$link.' target="_blank" rel="noopener">disini</a></strong> untuk melihat tutorialnya.');

		        if ($this->email->send()) {
		            $error = '';
					throw new Exception('Reset <b> PASSWORD </b> berhasil.');
		        } 
		        else 
		        {
		            $error = 1;
					throw new Exception('Email tidak terkirim '. $this->email->print_debugger());
		        }
			} catch (Exception $e) {
				$ds['error'] = $error;
				$ds['msg'] = $e->getMessage();
			}	

			$dt = array(
				'error' => $ds['error'],
				'msg' => $ds['msg']
			);
			echo json_encode($dt);
			//$this->instrument->generateReturn(base_url($this->uri->segment(1).'/login'));
	}

}
