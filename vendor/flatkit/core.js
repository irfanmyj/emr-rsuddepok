function editAkun() {
    $('#modalAuthUpdate').modal();
}

$(document).ready(function () {
    $('.alert').delay(5000).slideUp(3000, function () {
        $(this).remove();
    });

    if ($('.select2').length > 0) {
        $('.select2').select2();
    }

});

function convertRemToPixels(rem) {
    return parseFloat(rem) * parseFloat(getComputedStyle(document.documentElement).fontSize);
}

function convertPixelToRem(px) {
    return parseFloat(px) / parseFloat(getComputedStyle(document.documentElement).fontSize);
}

function rgbToHex(rgb) {
    var r, g, b;

    rgb = rgb.toString().replace('rgb(', '');
    rgb = rgb.toString().replace(')', '');
    rgb = rgb.toString().replace(' ', '');
    var x = rgb.split(',');

    r = parseInt(x[0]);
    g = parseInt(x[1]);
    b = parseInt(x[2]);

    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function checkPrivilege(aksesName) {

    if (aksesName == 0) {
        alert('Mohon maaf anda tidak memiliki hak untuk mengakses menu ini, silahkan hubungi administrator!...');
        return false;
    } 
    else{
        return true;
    }
}

function round(value, precision, mode) {
    var m, f, isHalf, sgn // helper variables
    // making sure precision is integer
    precision |= 0
    m = Math.pow(10, precision)
    value *= m
    // sign of the number
    sgn = (value > 0) | -(value < 0)
    isHalf = value % 1 === 0.5 * sgn
    f = Math.floor(value)
    if (isHalf) {
        switch (mode) {
            case 'PHP_ROUND_HALF_DOWN':
                // rounds .5 toward zero
                value = f + (sgn < 0)
                break
            case 'PHP_ROUND_HALF_EVEN':
                // rouds .5 towards the next even integer
                value = f + (f % 2 * sgn)
                break
            case 'PHP_ROUND_HALF_ODD':
                // rounds .5 towards the next odd integer
                value = f + !(f % 2)
                break
            default:
                // rounds .5 away from zero
                value = f + (sgn > 0)
        }
    }
    return (isHalf ? value : Math.round(value)) / m
}