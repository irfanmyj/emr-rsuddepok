//https://www.ibm.com/watson/developercloud/text-to-speech.html

//https://github.com/TalAter/annyang

//https://natural-language-classifier-demo.mybluemix.net/

//https://www.ibm.com/watson/developercloud/speech-to-text.html

window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition || null;
 
//caso não suporte esta API DE VOZ                              
if (window.SpeechRecognition === null) {
	document.getElementById('unsupported').classList.remove('hidden');
}else {
    var recognizer = new window.SpeechRecognition();
    var transcription = document.getElementById("voicetotexts");
	//Para o reconhecedor de voz, não parar de ouvir, mesmo que tenha pausas no usuario
	recognizer.continuous = true;
	recognizer.onresult = function(event){
		transcription.textContent = "";
		for (var i = event.resultIndex; i < event.results.length; i++) {
			if(event.results[i].isFinal){
				transcription.textContent = event.results[i][0].transcript;
			}else{
            	transcription.textContent += event.results[i][0].transcript;
			}
		}
		
		if(transcription.textContent!='')
		{
			var res = transcription.textContent;
			if(res.match('subjektif')=='subjektif' || res.match('subjective')=='subjective' || res.match('satu')=='satu' || res.match('1')=='1')
			{
				if(res.match('hapus')=='hapus' || res.match('salah')=='salah')
				{
					data = '';
				}
				else
				{
					var s = $('#subjektif').val();
					if(s.length > 0)
					{
						var textvoice = res;
						var subjective = $('#subjektif').val();
						var data = subjective +', '+ textvoice.replace(/subjektif/g,'').replace(/subjective/g,'').replace(/satu/g,'').replace(/1/g,'');
					}
					else
					{
						var textvoice = res;
						var data = textvoice.replace(/subjektif/g,'').replace(/subjective/g,'').replace(/satu/g,'');
					}
				}
				
				$('#subjektif').val(data);
				
			}
			else if(res.match('objective')=='objective' || res.match('objektif')=='objektif' || res.match('dua')=='dua')
			{
				if(res.match('hapus')=='hapus' || res.match('salah')=='salah')
				{
					data = '';
				}
				else
				{
					var s = $('#objective').val();
					if(s.length > 0)
					{
						var textvoice = res;
						var subjective = $('#objective').val();
						var data = subjective +', '+ textvoice.replace(/objective/g,'').replace(/objektif/g,'').replace(/dua/g,'');
					}
					else
					{
						var textvoice = res;
						var data = textvoice.replace(/objective/g,'').replace(/objektif/g,'').replace(/dua/g,'');
					}
				}
				
				$('#objective').val(data);
			}
			else if(res.match('pengkajian')=='pengkajian' || res.match('tiga')=='tiga')
			{
				if(res.match('hapus')=='hapus' || res.match('salah')=='salah')
				{
					data = '';
				}
				else
				{
					var s = $('#pengkajian').val();
					if(s.length > 0)
					{
						var textvoice = res;
						var subjective = $('#pengkajian').val();
						var data = subjective +', '+ textvoice.replace(/pengkajian/g,'').replace(/tiga/g,'');
					}
					else
					{
						var textvoice = res;
						var data = textvoice.replace(/pengkajian/g,'').replace(/tiga/g,'');
					}
				}
				
				$('#pengkajian').val(data);
			}
			else if(res.match('rencana')=='rencana' || res.match('plan')=='plan' || res.match('empat')=='empat')
			{
				if(res.match('hapus')=='hapus' || res.match('salah')=='salah')
				{
					data = '';
				}
				else
				{
					var s = $('#rencana').val();
					if(s.length > 0)
					{
						var textvoice = res;
						var subjective = $('#rencana').val();
						var data = subjective +', '+ textvoice.replace(/rencana/g,'').replace(/empat/g,'');
					}
					else
					{
						var textvoice = res;
						var data = textvoice.replace(/rencana/g,'').replace(/empat/g,'');
					}
				}
				
				$('#rencana').val(data);
			}
			else if(res.match('simpan')=='simpan')
			{
	        	var s = $('#formsimpan_pemeriksaan [name="s"]').val();
	        	var o = $('#formsimpan_pemeriksaan [name="o"]').val();
	        	var a = $('#formsimpan_pemeriksaan [name="a"]').val();
	        	var p = $('#formsimpan_pemeriksaan [name="p"]').val();
	        	var t = $('#formsimpan_pemeriksaan [name="timerDurasiSoap"]').val();

	        	if(s!='')
	        	{
	        		$.ajax({
		        		type : 'post',
		        		url : link+'/savepemeriksaan',
		        		data : 'no_rawat='+NoRawat+'&kd_dokter='+kd_dokter+'&kd_poli='+kd_poli+'&tgl='+TglPerawatan+'&s='+s+'&o='+o+'&a='+a+'&p='+p+'&t='+t,
		        		success : function(res)
		        		{
		        			/*alert(res);*/
		        			var r = JSON.parse(res);
		        			if(r.error=='')
		        			{
		        				$('[name="s"]').val(r.s);
		        				$('[name="o"]').val(r.o);
		        				$('[name="a"]').val(r.a);
		        				$('[name="p"]').val(r.p);
		        				$('#modalUmum').css('display','block');
		        				$('#msg').html(r.msg);
		        				$('#exampleModal2').modal();
		        				reset_timer();
		        			}
		        			else
		        			{
		        				$('#modalUmum').css('display','block');
		        				$('#msg').html(r.msg);
		        				$('#exampleModal2').modal();
		        			}
		        		}
		        	});
	        	}
	        	else
	        	{
	        		alert('Data tidak boleh kosong.');
	        	}
			}

			else
			{
				alert('tidak ditemukan form untuk menyimpan data ini '+ res);
			}
			
		}
	}

	document.querySelector("#rect").addEventListener("click",function(){
		try {
            recognizer.start();
          } catch(ex) {
          	alert("error: "+ex.message);
          }
	});

	document.querySelector("#rectstop").addEventListener("click",function(){
		try {
            recognizer.stop();
          } catch(ex) {
          	alert("error: "+ex.message);
          }
	});
}