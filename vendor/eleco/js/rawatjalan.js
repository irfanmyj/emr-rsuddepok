	$(document).ready(function(){
		
		/*$("#soap").collapse('show');
		$("#fdp").collapse('show');
		$("#frp").collapse('show');
		$("#frl").collapse('show');
		$("#fpr").collapse('show');
		$("#fptn").collapse('show');*/

		/*
		START AREA TIMER FORMULIR
		*/
		$('#perawat_tabs-tab').click(function(){
			reset_timer();
	        clearInterval(SI);
	   	});

		$('#soap-tab').click(function(){
			reset_timer();
	        clearInterval(SI);
	        SI = setInterval("startTimer($('#timerDurasiSoap'))",1000);
	   	});

		$('#diagnosa-tab').click(function(){
			reset_timer();
	        clearInterval(SI);
	        SI = setInterval("startTimer($('#timerDurasiDiagnosa'))",1000);
	   	});

	   	$('#resepobat-tab').click(function(){
	   		reset_timer();
	        clearInterval(SI);
	        SI = setInterval("startTimer($('#timerDurasiObat'))",1000);
	   	});
		
		$('#laboratorium-tab').click(function(){
	   		reset_timer();
	        clearInterval(SI);
	        SI = setInterval("startTimer($('#timerDurasiLab'))",1000);
	   	});

	   	$('#radiologi_tabs-tab').click(function(){
	   		reset_timer();
	        clearInterval(SI);
	        SI = setInterval("startTimer($('#timerDurasiRad'))",1000);
	   	});

	   	$('#tindakan_tabs-tab').click(function(){
	   		reset_timer();
	        clearInterval(SI);
	        SI = setInterval("startTimer($('#timerDurasiTindakan'))",1000);
	   	});
	   	/*
		END AREA TIMER FORMULIR
		*/

		$('.search2').select2();

		// Area awal untuk menampilkan history pasien
		$('#no_rawat').change(function(){
			var no_rawat 		= $(this).val();
			var no_rkm_medis 	= $('#no_rkm_medis').val();
			if(no_rawat)
			{
				$.ajax({
					type	: 'post',
					url 	: link+'/getHistory',
					data 	: 'no_rawat='+no_rawat+'&no_rkm_medis='+no_rkm_medis,
					success : function(res)
					{
						var data_view = JSON.parse(res);
						$('#nav-tabs1').html(data_view.poliklinik);
						$('#nav-tabs2').html(data_view.rawatinap);
					}
				});
			}
			else
			{
				$('#nav-tabs1').html('<span class="text-center p-2"><p class="text-justify"><h5>Data tidak ditemukan.</h5></p></span>');
				$('#nav-tabs2').html('<span class="text-center p-2"><p class="text-justify"><h5>Data tidak ditemukan.</h5></p></span>');
			}
		});
		// area akhir

		// Area diagnosa 
		$("#getdiagnosa").autocomplete({
            source: "<?php echo site_url($this->uri->segment(1).'/getDiagnosa');?>",
            minLength: 3,
            focus: function( event, ui ) {
            	event.preventDefault();
            	$(this).val(ui.item.label);
            },
            select: function(event, ui) {
                //console.log(event);
                $(this).val('');//.blur();
                event.preventDefault(); // cancel default behavior which updates input field
                
                $("#list-datadignosa").append('\n\
                    <tr id="'+ui.item.keys+'">\n\
                        <td class="tr_mod"><input type="hidden" name="kd_penyakit[]" value='+ui.item.id+'><h6>'+ui.item.value+'</h6></td>\n\
                        <td style="cursor:pointer;" class="delete" data-id="'+ui.item.keys+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
                    </tr>');
            }
        });

        $('#formsimpan_diagnosa').on('click','#simpandiagnosa', function(){
        	var diagnosa = $('[name="kd_penyakit[]"]').serialize();
        	$("#list-datadignosa tr").remove();
        	if(diagnosa!='')
        	{
        		$.ajax({
	        		type : 'post',
	        		url : link+'/savediagnosa',
	        		data : diagnosa+'&no_rawat='+NoRawat+'&status_penyakit='+status_poli,
	        		success : function(res)
	        		{
	        			var r = JSON.parse(res);
	        			if(r.error=='')
	        			{
	        				showmodal('Data berhasil disimpan.');
	        				$('#formsimpan_diagnosa .showdiagnosa').html(r.view);
	        				delete_diagnosa();
	        				sortTableDiag2();
	        			}
	        			else
	        			{
	        				showmodal('Data gagal disimpan.');
	        				$('#formsimpan_diagnosa .showdiagnosa').html(r.view);
	        			}
	        		}
	        	});
        	}
        	else
        	{
        		alert('Data tidak boleh kosong.');
        	}
        });

        $( ".tabledianosa1" ).sortable({
	        delay: 150,
	        stop: function() {
	            var selectedData = new Array();
	            $('.tabledianosa1>tr').each(function() {
	                selectedData.push($(this).attr("id"));
	            });
	            updateOrder(selectedData,NoRawat);
	        }
	    });

	    $('#list-datadignosa').on('click', '.delete', function(){
            var tr = $(this).data('id');
            $('#'+tr).remove();
        });

        $(function(){
		    $( "#list-datadignosa" ).sortable();
		    $( "#list-datadignosa" ).disableSelection();
	  	});

	  	sortTableDiag2();
		// Area akhir diagnosa

		// Area awal module laboratorium
        $("#laboratorium").autocomplete({
            source: "<?php echo site_url($this->uri->segment(1).'/getLaboratorium');?>",
            minLength: 3,
            focus: function( event, ui ) {
            	event.preventDefault();
            	$(this).val(ui.item.label);
            },
            select: function(event, ui) {
                //console.log(event);
                $(this).val('');//.blur();
                event.preventDefault(); // cancel default behavior which updates input field
                
                $("#labs").append('\n\
                    <tr id="'+ui.item.id+'">\n\
                        <td class="tr_mod"><input type="hidden" name="kd_jenis_prw_labs[]" value='+ui.item.id+'><h6>'+ui.item.value+'</h6></td>\n\
                        <td style="cursor:pointer;" class="delete" data-id="'+ui.item.id+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
                    </tr>');
            }
        });

        $('#labs').on('click', '.delete', function(){
            var tr = $(this).data('id');
            $('table#labs tr#'+tr).remove();
            //$('#labs tr #').remove();
        });

        $('#formsimpan_laboratorium').on('click','#simpanlabs', function(){
        	var labs = $('[name="kd_jenis_prw_labs[]"]').serialize();
        	$("#labs tr").remove();
        	if(labs!='')
        	{
        		$.ajax({
	        		type : 'post',
	        		url : link+'/savelabs',
	        		data : labs+'&no_rawat='+NoRawat+'&kd_dokter='+kd_dokter+'&tgl='+TglPerawatan,
	        		success : function(res)
	        		{
	        			//alert(res);
	        			var r = JSON.parse(res);
	        			if(r.error=='')
	        			{
	        				showmodal('Data berhasil disimpan.');
	        				$('#formsimpan_laboratorium .showlabs').html(r.view);
	        				delete_labs();
	        			}
	        			else
	        			{
	        				showmodal('Data gagal disimpan.');
	        				$('#formsimpan_laboratorium .showlabs').html(r.view);
	        			}
	        		}
	        	});
        	}
        	else
        	{
        		alert('Data tidak boleh kosong.');
        	}
        });

        delete_labs();
        // Area akhir module laboratorium

        $("#radiologi").autocomplete({
            source: "<?php echo site_url($this->uri->segment(1).'/getRadiologi');?>",
            minLength: 3,
            focus: function( event, ui ) {
            	event.preventDefault();
            	$(this).val(ui.item.label);
            },
            select: function(event, ui) {
                //console.log(event);
                $(this).val('');//.blur();
                event.preventDefault(); // cancel default behavior which updates input field
                
                $("#rads").append('\n\
                    <tr id="'+ui.item.id+'">\n\
                        <td class="tr_mod"><input type="hidden" name="kd_jenis_prw_rad[]" value='+ui.item.id+'><h6>'+ui.item.value+'</h6></td>\n\
                        <td style="cursor:pointer;" class="delete" data-id="'+ui.item.id+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
                    </tr>');
            }
        });

        $('#rads').on('click', '.delete', function(){
            var tr = $(this).data('id');
           $('table#rads tr#'+tr).remove();
        });

        $("#tindakan").autocomplete({
            source: function(request, response){
            	$.ajax({
            		url : "<?php echo site_url($this->uri->segment(1).'/getTindakan');?>",
            		dataType: "json",
            		data : {
            			nama_perawatan : request.term,
            			kd_pj : kd_pj,
            			kd_poli : kd_poli
            		},
            		success : function(data)
            		{
            			response(data);
            		}
            	});
            },
            minLength: 3,
            focus: function( event, ui ) {
            	event.preventDefault();
            	$(this).val(ui.item.label);
            },
            select: function(event, ui) {
                //console.log(event);
                $(this).val('');//.blur();
                event.preventDefault(); // cancel default behavior which updates input field
                
                $("#tind").append('\n\
                    <tr id="'+ui.item.id+'">\n\
                        <td class="tr_mod">\n\
                        <input type="hidden" name="kd_jenis_prw_tindakan[]" value='+ui.item.id+'>\n\
                        <input type="hidden" name="material[]" value='+ui.item.material+'>\n\
                        <input type="hidden" name="bhp[]" value='+ui.item.bhp+'>\n\
                        <input type="hidden" name="tarif_tindakanpr[]" value='+ui.item.tarif_tindakanpr+'>\n\
                        <input type="hidden" name="tarif_tindakandr[]" value='+ui.item.tarif_tindakanpr+'>\n\
                        <input type="hidden" name="kso[]" value='+ui.item.kso+'>\n\
                        <input type="hidden" name="menejemen[]" value='+ui.item.menejemen+'>\n\
                        <input type="hidden" name="biaya_rawat[]" value='+ui.item.biaya_rawat+'>\n\
                        <h6>'+ui.item.value+'</h6>\n\
                        </td>\n\
                        <td style="cursor:pointer;" class="delete" data-id="'+ui.item.id+'"><h6><i class="fa fa-delete"> Hapus</i></h6></td>\n\
                    </tr>');
            }
        });

        $('#tind').on('click', '.delete', function(){
            var tr = $(this).data('id');
           $('table#tind tr#'+tr).remove();
        });

        $('.add-row').click(function(){
        	var rowobat = $('#tb-resepobat tr').length;
        	var inputId = $('#tb-resepobat tr').last().attr('id');
        	var counter = 1;
        	
        	if(rowobat >= 0)
        	{
        		$('#add-obat').css('display','block');
        		$('#simpanobat').css('display','block');
        	}

        	if(inputId)
        	{
        		inputId++;
        		var datas = '<tr class="addtr" id='+inputId+'>\n\
        				<td width="50"><input type="checkbox" class="form-control" style="width:20px;" name="record"></td>\n\
						<td>\n\
						<select name="kode_obat[]" class="kd_obat form-control" style="width:100%"></select>\n\
						</td>\n\
						<td width="100">\n\
						<input type="number" name="jlm_obat[]" list="jumlahobat" class="form-control jlm_obat">\n\
						</td>\n\
						<td width="100">\n\
						<textarea name="keterangan[]" class="form-control"></textarea>\n\
						</td>\n\
						<td width="200"><?php echo  $this->instrument->htmlSelectFromArray($aturan_pakai, 'name="aturan_pakai[]" id="kd_poli" class="form-control aturan_pakai"', true);?>\n\
						</td>\n\
						</tr>';
						
        	}
        	else
        	{
        		var datas = '<tr class="addtr" id='+counter+' style="">\n\
    				<td width="50"><input type="checkbox" class="form-control" style="width:20px;" name="record"></td>\n\
					<td>\n\
					<select name="kode_obat[]" class="kd_obat form-control" style="width:100%"></select>\n\
					</td>\n\
					<td width="100">\n\
					<input type="number" name="jlm_obat[]" list="jumlahobat" class="form-control jlm_obat">\n\
					</td>\n\
					<td width="100">\n\
					<textarea name="keterangan[]" class="form-control"></textarea>\n\
					</td>\n\
					<td width="200"><?php echo  $this->instrument->htmlSelectFromArray($aturan_pakai, 'name="aturan_pakai[]" id="kd_poli" class="form-control aturan_pakai"', true);?>\n\
					</td>\n\
					</tr>';
        	} 	  
        	
			$('.tblInputO tbody').append(datas);

			$('.tblInputO tbody .kd_obat').select2({
				minimumInputLength: 3,
				allowClear: true,
				placeholder: 'Pilih obat',
				ajax: 
				{
					url: '<?php echo site_url($this->uri->segment(1).'/getObat');?>',
					dataType: 'json',
					delay: 250,
					data : function(params)
					{
						return {
		                  search: params.term
		                }
					},
					processResults: function (data, page) 
					{
						return {
							results: data
						};
					},
				}
	        }).on('select2:select', function (evt) {
		         var data = $(".kd_obat option:selected").val();
		         //alert("Data yang dipilih adalah "+ data);
		    });

	        //$('.tblInputO tbody .aturan_pakai').change(function(){
	        //	var jlm_obat = $('[name="jlm_obat[]"]').serialize();
	        //	var kode_obat = $('[name="kode_obat[]"]').serialize();
	        //	var aturan_pakai = $('[name="aturan_pakai[]"]').serialize();
	        //	var keterangan = $('[name="keterangan[]"]').serialize();
	        //	$.ajax({
	        //		type : 'post',
	        //		url : link+'/getHargaObat',
	        //		data : jlm_obat+'&'+kode_obat+'&tgl_perawatan='+TglPerawatan+'&no_rawat='+NoRawat+'&'+keterangan+'&'+aturan_pakai,
	        //		success : function(res)
	        //		{
	        //			$('#biling_pasien').html(res);
	        //		}
	        //	});
	        //});

	        $(".del").click(function(){
		        $(".tblInputO tbody").find('input[name="record"]').each(function(){
	            	if($(this).is(":checked")){
	            		$(this).parents("tr").remove();
	                }
	            });
		    }); 
        });

	    // Simpan data obat
	    $('#formsimpan_obat').on('click','#simpanobat', function(){
	    	var jlm_obat = $('[name="jlm_obat[]"]').serialize();
        	var kode_obat = $('[name="kode_obat[]"]').serialize();
        	var aturan_pakai = $('[name="aturan_pakai[]"]').serialize();
        	var keterangan = $('[name="keterangan[]"]').serialize();
	    	$("#tb-resepobat tr").remove();
	    	if(kode_obat!='' && jlm_obat!='' && aturan_pakai!='')
	    	{
	    		$.ajax({
	        		type : 'post',
	        		url : link+'/saveResepObat1',
	        		data : jlm_obat+'&'+kode_obat+'&tgl_perawatan='+TglPerawatan+'&no_rawat='+NoRawat+'&'+keterangan+'&'+aturan_pakai+'&kd_dokter='+kd_dokter,
	        		success : function(res)
	        		{
	        			var r = JSON.parse(res);
	        			if(r.error=='')
	        			{
	        				showmodal('Data berhasil disimpan.');
	        				$('#formsimpan_obat .showresepobat').html(r.view);
	        				delete_resepobat();
	        			}
	        			else
	        			{
	        				showmodal('Data gagal disimpan.');
	        				$('#formsimpan_obat .showresepobat').html(r.view);
	        			}
	        		}
	        	});
	    	}	    	
	    })       

        // Simpan data Radiologi
        $('#formsimpan_radiologi').on('click','#simpanrads', function(){
        	var rads = $('[name="kd_jenis_prw_rad[]"]').serialize();
        	$("#rads tr").remove();
        	if(rads!='')
        	{
        		$.ajax({
	        		type : 'post',
	        		url : link+'/saverads',
	        		data : rads+'&no_rawat='+NoRawat+'&kd_dokter='+kd_dokter+'&tgl='+TglPerawatan,
	        		success : function(res)
	        		{
	        			//alert(res);
	        			var r = JSON.parse(res);
	        			if(r.error=='')
	        			{
	        				showmodal('Data berhasil disimpan.');
	        				$('#formsimpan_radiologi .showrads').html(r.view);
	        				delete_rads();
	        			}
	        			else
	        			{
	        				showmodal('Data gagal disimpan.');
	        				$('#formsimpan_radiologi .showrads').html(r.view);
	        			}
	        		}
	        	});
        	}
        	else
        	{
        		alert('Data tidak boleh kosong.');
        	}
        });

        // Simpan data Tindakan
        $('#formsimpan_tindakan').on('click','#simpantindakan', function(){
        	var tind = $('[name="kd_jenis_prw_tindakan[]"]').serialize();
        	var material = $('[name="material[]"]').serialize();
        	var bhp = $('[name="bhp[]"]').serialize();
        	var tarif_tindakandr = $('[name="tarif_tindakandr[]"]').serialize();
        	var tarif_tindakanpr = $('[name="tarif_tindakanpr[]"]').serialize();
        	var kso = $('[name="kso[]"]').serialize();
        	var menejemen = $('[name="menejemen[]"]').serialize();
        	var biaya_rawat = $('[name="biaya_rawat[]"]').serialize();
        	$("#tind tr").remove();
        	if(tind!='')
        	{
        		$.ajax({
	        		type : 'post',
	        		url : link+'/savetind',
	        		data : tind+'&'+material+'&'+bhp+'&'+tarif_tindakandr+'&'+tarif_tindakanpr+'&'+kso+'&'+menejemen+'&'+biaya_rawat+'&no_rawat='+NoRawat+'&kd_dokter='+kd_dokter+'&tgl='+TglPerawatan,
	        		success : function(res)
	        		{
	        			//alert(res);
	        			var r = JSON.parse(res);
	        			if(r.error=='')
	        			{
	        				showmodal('Data berhasil disimpan.');
	        				$('#formsimpan_tindakan .showtindakan').html(r.view);
	        				delete_tindakan();
	        			}
	        			else
	        			{
	        				showmodal('Data gagal disimpan.');
	        				$('#formsimpan_tindakan .showtindakan').html(r.view);
	        			}
	        		}
	        	});
        	}
        	else
        	{
        		alert('Data tidak boleh kosong.');
        	}
        });

        // Simpan data Radiologi
        $('#formsimpan_pemeriksaan').on('click','#simpanpemeriksaan', function(){
        	var s = $('[name="s"]').val();
        	var o = $('[name="o"]').val();
        	var a = $('[name="a"]').val();
        	var p = $('[name="p"]').val();
        	var t = $('[name="timerDurasiSoap"]').val();
        	if(s!='')
        	{
        		$.ajax({
	        		type : 'post',
	        		url : link+'/savepemeriksaan',
	        		data : 'no_rawat='+NoRawat+'&kd_dokter='+kd_dokter+'&kd_poli='+kd_poli+'&tgl='+TglPerawatan+'&s='+s+'&o='+o+'&a='+a+'&p='+p+'&t='+t,
	        		success : function(res)
	        		{
	        			/*alert(res);*/
	        			var r = JSON.parse(res);
	        			if(r.error=='')
	        			{
	        				$('[name="s"]').val(r.s);
	        				$('[name="o"]').val(r.o);
	        				$('[name="a"]').val(r.a);
	        				$('[name="p"]').val(r.p);
	        				$('#modalUmum').css('display','block');
	        				$('#msg').html(r.msg);
	        				$('#exampleModal2').modal();
	        			}
	        			else
	        			{
	        				$('#modalUmum').css('display','block');
	        				$('#msg').html(r.msg);
	        				$('#exampleModal2').modal();
	        			}
	        		}
	        	});
        	}
        	else
        	{
        		alert('Data tidak boleh kosong.');
        	}
        });

        $('#datapemeriksaan').on('click','.edit-pemeriksaan',function(){
        	$.ajax({
        		type : 'post',
        		url  : link+'/editpemeriksaan',
        		data : 'NoRawat='+NoRawat+'&edit=edit',
        		success : function(res)
        		{
        			$('#datapemeriksaan').html(res);
        		} 
        	});
        });

        $('#datapemeriksaan').on('click','.update-pemeriksaan',function(){
        	var data = $('#datapemeriksaan :input').serialize();
        	$.ajax({
        		type : 'post',
        		url  : link+'/updatepemeriksaan',
        		data : 'no_rawat='+NoRawat+'&'+data,
        		success : function(res)
        		{
        			var d = JSON.parse(res);
        			if(d.error==1)
        			{
        				$('#modalUmum').css('display','block');
        				$('#msg').html(d.html);
        				$('#exampleModal2').modal();
        			}
        			else
        			{
        				$('#modalUmum').css('display','block');
        				$('#msg').html('Data berhasil disimpan');
        				$('#exampleModal2').modal();
        				$('#datapemeriksaan').html(d.html);
        			}
        		} 
        	});
        });

        $('#datapemeriksaan').on('click','.cancel-pemeriksaan',function(){
        	var data = $('#datapemeriksaan :input').serialize();
        	$.ajax({
        		type : 'post',
        		url  : link+'/editpemeriksaan',
        		data : 'NoRawat='+NoRawat,
        		success : function(res)
        		{
        			$('#datapemeriksaan').html(res);
        		} 
        	});
        });

        delete_diagnosa();
        delete_resepobat();
        delete_rads();
        delete_tindakan();
	});


	function formatData (data) {
		var ret;
		if(data.stok <= 0)
    	{
    		ret = '<span class="text-primary">'+ data.text+'</span>';
    	}
    	else
    	{
    		ret = '<span>'+ data.text +'</span>';
    	}
        var $data = $(
        	ret
        );
        return $data;
    };

	function filterTable(event) {
	    var filter = event.target.value.toUpperCase();
	    var rows = document.querySelector("#myTable tbody").rows;
	    
	    for (var i = 0; i < rows.length; i++) {
	        var firstCol = rows[i].cells[0].textContent.toUpperCase();
	        var secondCol = rows[i].cells[1].textContent.toUpperCase();
	        if (firstCol.indexOf(filter) > -1 || secondCol.indexOf(filter) > -1) {
	            rows[i].style.display = "";
	        } else {
	            rows[i].style.display = "none";
	        }      
	    }
	}

	function updateOrder(data,norawat) {
        $.ajax({
            url:"<?php echo base_url($this->uri->segment(1));?>/updateDiagnosa",
            type:'post',
            data:'kd_penyakit='+data+'&no_rawat='+norawat,
            success:function(res){
            	var r = JSON.parse(res);
            	if(r.error=='')
            	{
            		//alert('perubahan Anda berhasil disimpan');
            		$('#formsimpan_diagnosa .showdiagnosa').html(r.view);
            		delete_diagnosa();
            		sortTableDiag2();
            	}
            }
        })
    }

    function delete_diagnosa()
    {
    	$('.delete_diagnosa').click(function(){
	    	var tr = $(this).parent();
	    	if(tr.data('kd_penyakit')!='')
	    	{
	    		$.ajax({
	    			type : 'post',
	    			url : link+'/deleteDiagnosa',
	    			data : 'kd_penyakit='+tr.data('kd_penyakit')+'&no_rawat='+tr.data('no_rawat'),
	    			success : function(res)
	    			{
	    				var r = JSON.parse(res);
	    				if(r.error=='')
	    				{
	    					showmodal('Data berhasil dihapus.');
	    					$('#formsimpan_diagnosa .showdiagnosa').html(r.view);
	        				delete_diagnosa();
	        				$( "#formsimpan_diagnosa .showdiagnosa .tabledianosa2" ).sortable({
						        delay: 150,
						        stop: function() {
						            var selectedData = new Array();
						            $('.tabledianosa2>tr').each(function() {
						                selectedData.push($(this).attr("id"));
						            });
						            updateOrder(selectedData,NoRawat);
						        }
						    });
	    				}
	    				else
	    				{
	    					showmodal('Data gagal dihapus.');
	    				}
	    			}
	    		});
	    	}
	    });
    }

    function delete_resepobat()
    {
    	$('.deleteResepObat').click(function(){
	    	var tr = $(this).parent();
	    	if(tr.data('no_rawat')!='' && tr.data('kode_brng')!='')
	    	{
	    		$.ajax({
	    			type : 'post',
	    			url : link+'/deleteResepobat1',
	    			data : 'no_rawat='+tr.data('no_rawat')+'&kode_brng='+tr.data('kode_brng')+'&no_resep='+tr.data('no_resep'),
	    			success : function(res)
	    			{
	    				var r = JSON.parse(res);
	    				if(r.error=='')
	    				{
	    					showmodal('Data berhasil dihapus.');
	    					$('#formsimpan_obat .showresepobat').html(r.view);
	    					delete_resepobat();
	    				}
	    				else
	    				{
	    					showmodal('Data gagal dihapus.');
	    				}
	    			}
	    		});
	    	}
	    });
    }

    function delete_labs()
    {
    	$('.deletelabs').click(function(){
	    	var tr = $(this).parent();
	    	if(tr.data('kd_jenis_prw')!='')
	    	{
	    		$.ajax({
	    			type : 'post',
	    			url : link+'/deletelabs',
	    			data : 'kd_jenis_prw='+tr.data('kd_jenis_prw')+'&no_rawat='+tr.data('no_rawat')+'&noorder='+tr.data('noorder'),
	    			success : function(res)
	    			{
	    				var r = JSON.parse(res);
	    				if(r.error=='')
	    				{
	    					showmodal('Data berhasil dihapus.');
	    					$('#formsimpan_laboratorium .showlabs').html(r.view);
	    					delete_labs();
	    				}
	    				else
	    				{
	    					showmodal('Data gagal dihapus.');
	    				}
	    			}
	    		});
	    	}
	    	else
	    	{
	    		alert('Mohon maaf anda tidak bisa menghapus.')
	    	}
	    });
    }

    function delete_rads()
    {
    	$('.deleterads').click(function(){
	    	var tr = $(this).parent();
	    	if(tr.data('kd_jenis_prw')!='')
	    	{
	    		$.ajax({
	    			type : 'post',
	    			url : link+'/deleterads',
	    			data : 'kd_jenis_prw='+tr.data('kd_jenis_prw')+'&no_rawat='+tr.data('no_rawat')+'&noorder='+tr.data('noorder'),
	    			success : function(res)
	    			{
	    				var r = JSON.parse(res);
	    				if(r.error=='')
	    				{
	    					showmodal('Data berhasil dihapus.');
	    					$('#formsimpan_radiologi .showrads').html(r.view);
	    					delete_rads();
	    				}
	    				else
	    				{
	    					showmodal('Data gagal dihapus.');
	    				}
	    				
	    			}
	    		});
	    	}
	    	else
	    	{
	    		alert('Mohon maaf anda tidak bisa menghapus.')
	    	}
	    });
    }

    function delete_tindakan()
    {
    	$('.deletetind').click(function(){
	    	var tr = $(this).parent();
	    	if(tr.data('kd_jenis_prw')!='')
	    	{
	    		$.ajax({
	    			type : 'post',
	    			url : link+'/deletetindakan',
	    			data : 'kd_jenis_prw='+tr.data('kd_jenis_prw')+'&no_rawat='+tr.data('no_rawat'),
	    			success : function(res)
	    			{
	    				var r = JSON.parse(res);
	    				if(r.error=='')
	    				{
	    					showmodal('Data berhasil dihapus.');
	    					$('#formsimpan_tindakan .showtindakan').html(r.view);
	    					delete_tindakan();
	    				}
	    				else
	    				{
	    					showmodal('Data gagal dihapus.');
	    				}
	    				
	    			}
	    		});
	    	}
	    	else
	    	{
	    		alert('Mohon maaf anda tidak bisa menghapus.')
	    	}
	    });
    }

    function sortTableDiag2()
    {
    	$( ".tablediagnosa2" ).sortable({
	        delay: 150,
	        stop: function() {
	            var selectedData = new Array();
	            $('.tablediagnosa2>tr').each(function() {
	                selectedData.push($(this).attr("id"));
	            });
	            updateOrder(selectedData,NoRawat);
	        }
	    });
    }

    function showmodal(pesan)
    {
    	$('#modalUmum').css('display','block');
		$('#msg').html(pesan);
		$('#exampleModal2').modal();
    }

    function startTimer(selector){
	    var ini     = selector
	    if(ini.val()!='' && ini.val()!=null){
	        var split   = ini.val().split(':');
	        var detik   = split[2];
	        var menit   = split[1];
	        var jam     = split[0];
	        
	        detik++;
	        if(detik == 60){
	            menit++;
	            detik = '00';
	            if(menit == 60){
	                jam++;
	                menit = '00';
	            }
	        }
	        
	        if(detik.toString().length == 1){
	           detik = '0'+detik ;
	        }
	        if(menit.toString().length == 1){
	           menit = '0'+menit ;
	        }
	        if(jam.toString().length == 1){
	           jam = '0'+jam ;
	        }
	        
	        ini.val(jam+':'+menit+':'+detik);
	    }
	}

	function reset_timer()
	{
		$('#timerDurasiDiagnosa').val('00:00:00');
		$('#timerDurasiSoap').val('00:00:00');
		$('#timerDurasiObat').val('00:00:00');
		$('#timerDurasiLab').val('00:00:00');
		$('#timerDurasiRad').val('00:00:00');
		$('#timerDurasiTindakan').val('00:00:00');
	}

    function clock() {
      var now = new Date();
      var secs = ('0' + now.getSeconds()).slice(-2);
      var mins = ('0' + now.getMinutes()).slice(-2);
      var hr = now.getHours();
      var Time = hr + ":" + mins + ":" + secs;
      document.getElementById("watch").innerHTML = 'Jam : '+Time;
      requestAnimationFrame(clock);
    }
	requestAnimationFrame(clock);

	document.querySelector('#myInput').addEventListener('keyup', filterTable, false);

	var kedipan = 750; 
	var ele = document.getElementById('ralert')
	if(ele)
	{
		var dumet = setInterval(function () {
		    ele.style.visibility = (ele.style.visibility == 'hidden' ? '' : 'hidden');
		}, kedipan);
	}